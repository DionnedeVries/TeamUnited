--- 
title: First post
date: 2017-10-06
---
# Voorbeeld post

Dit is een voorbeeld post voor de workshop **bloggen**.
Wat we hebben gedaan is:
1. Gitlab aangemaakt
2. Hugo geforkt
3. Settings aangepast
4. Blogs verwijderd, wat heel veel werk was
5. Een nieuwe post online gezet, deze

Wat we nog moeten doen:
* Thema wijzigen
* nog meer settings aanpassen
* Meer bloggen
* _Iets wat cursief is_
* En nog meer
